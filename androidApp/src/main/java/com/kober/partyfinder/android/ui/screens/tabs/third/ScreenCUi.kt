package com.kober.partyfinder.android.ui.screens.tabs.third

import androidx.compose.runtime.Composable
import com.arkivanov.decompose.ExperimentalDecomposeApi
import com.arkivanov.decompose.extensions.compose.jetpack.Children
import com.arkivanov.decompose.extensions.compose.jetpack.animation.child.crossfade
import com.kober.partyfinder.components.tabs.third.IScreenC

@ExperimentalDecomposeApi
@Composable
fun ScreenCUi(screenC: IScreenC) {
    Children(
        routerState = screenC.routerState,
        animation = crossfade()
    ) {
        when (val child = it.instance) {
            is IScreenC.Child.ScreenC1 -> ScreenC1Ui(child.component)
            is IScreenC.Child.ScreenC2 -> ScreenC2Ui(child.component)
        }
    }
}