package com.kober.partyfinder.android.ui.screens

import androidx.compose.runtime.Composable
import com.arkivanov.decompose.ExperimentalDecomposeApi
import com.arkivanov.decompose.extensions.compose.jetpack.Children
import com.arkivanov.decompose.extensions.compose.jetpack.animation.child.crossfade
import com.arkivanov.decompose.extensions.compose.jetpack.animation.child.slide
import com.kober.partyfinder.components.root.IRoot

@ExperimentalDecomposeApi
@Composable
fun RootUi(root: IRoot) {
    Children(
        routerState = root.routerState,
        animation = crossfade()
    ) {
        when (val child = it.instance) {
            is IRoot.Child.Main -> MainUi(child.component)
        }
    }
}