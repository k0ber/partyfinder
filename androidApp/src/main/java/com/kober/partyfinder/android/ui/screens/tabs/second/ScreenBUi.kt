package com.kober.partyfinder.android.ui.screens.tabs.second

import androidx.compose.runtime.Composable
import com.arkivanov.decompose.ExperimentalDecomposeApi
import com.arkivanov.decompose.extensions.compose.jetpack.Children
import com.arkivanov.decompose.extensions.compose.jetpack.animation.child.crossfade
import com.kober.partyfinder.components.tabs.second.IScreenB

@ExperimentalDecomposeApi
@Composable
fun ScreenBUi(screenB: IScreenB) {
    Children(
        routerState = screenB.routerState,
        animation = crossfade()
    ) {
        when (val child = it.instance) {
            is IScreenB.Child.ScreenB1 -> ScreenB1Ui(child.component)
            is IScreenB.Child.ScreenB2 -> ScreenB2Ui(child.component)
        }
    }
}