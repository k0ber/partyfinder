package com.kober.partyfinder.android.ui.screens.tabs.first

import androidx.compose.runtime.Composable
import com.arkivanov.decompose.ExperimentalDecomposeApi
import com.arkivanov.decompose.extensions.compose.jetpack.Children
import com.arkivanov.decompose.extensions.compose.jetpack.animation.child.crossfade
import com.arkivanov.decompose.extensions.compose.jetpack.animation.child.slide
import com.kober.partyfinder.components.tabs.first.IScreenA

@ExperimentalDecomposeApi
@Composable
fun ScreenAUi(screenA: IScreenA) {
    Children(
        routerState = screenA.routerState,
        animation = slide()
    ) {
        when (val child = it.instance) {
            is IScreenA.Child.ScreenA1 -> ScreenA1Ui(child.component)
            is IScreenA.Child.ScreenA2 -> ScreenA2Ui(child.component)
        }
    }
}