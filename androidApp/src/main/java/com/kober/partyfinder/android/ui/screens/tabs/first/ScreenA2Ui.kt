package com.kober.partyfinder.android.ui.screens.tabs.first

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.kober.partyfinder.components.tabs.first.screena2.IScreenA2

@Composable
fun ScreenA2Ui(screenA2: IScreenA2) {
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Column {
            Text(
                text = "Screen A2"
            )
        }
    }
}